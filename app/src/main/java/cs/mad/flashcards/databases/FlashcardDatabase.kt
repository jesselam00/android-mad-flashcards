package cs.mad.flashcards.databases

import androidx.room.Database
import androidx.room.RoomDatabase
import cs.mad.flashcards.entities.Flashcard
import cs.mad.flashcards.entities.FlashcardDao

@Database(entities = [Flashcard::class], version = 1)
abstract class FlashcardDatabase: RoomDatabase() {
    companion object {
        const val databaseName = "FLASHCARD_DATABASE"
    }

    abstract fun cardDao() : FlashcardDao
}
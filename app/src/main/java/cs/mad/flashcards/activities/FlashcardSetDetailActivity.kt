package cs.mad.flashcards.activities

import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.room.Room
import cs.mad.flashcards.adapters.FlashcardAdapter
import cs.mad.flashcards.adapters.FlashcardSetAdapter
import cs.mad.flashcards.databases.FlashcardDatabase
import cs.mad.flashcards.databinding.ActivityFlashcardSetDetailBinding
import cs.mad.flashcards.entities.Flashcard
import cs.mad.flashcards.entities.FlashcardDao
import cs.mad.flashcards.runOnIO
import kotlin.random.Random

class FlashcardSetDetailActivity : AppCompatActivity() {
    private lateinit var binding: ActivityFlashcardSetDetailBinding
    private lateinit var cardDao: FlashcardDao

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityFlashcardSetDetailBinding.inflate(layoutInflater)
        val view = binding.root
        setContentView(view)

        val db = Room.databaseBuilder(
            applicationContext,
            FlashcardDatabase::class.java,FlashcardDatabase.databaseName
        ).build()
        cardDao = db.cardDao()

        binding.flashcardList.adapter = FlashcardAdapter(cardDao)


        binding.addFlashcardButton.setOnClickListener {
            runOnIO {
                cardDao.insert(Flashcard("test", "test",Random.nextInt(0,99999)))
            }
            (binding.flashcardList.adapter as FlashcardAdapter).addItem(Flashcard("test", "test",123))
                binding.flashcardList.smoothScrollToPosition((binding.flashcardList.adapter as FlashcardAdapter).itemCount - 1)
        }

        binding.studySetButton.setOnClickListener {
            startActivity(Intent(this, StudySetActivity::class.java))
        }

        binding.deleteSetButton.setOnClickListener {
            finish()
        }

        runOnIO {
            (binding.flashcardList.adapter as FlashcardAdapter).update(cardDao.getAll())
        }

    }
}
package cs.mad.flashcards.activities

import android.content.SharedPreferences
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.room.Room
import cs.mad.flashcards.adapters.FlashcardSetAdapter
import cs.mad.flashcards.databases.FlashcardSetDatabase
import cs.mad.flashcards.databinding.ActivityMainBinding
import cs.mad.flashcards.entities.FlashcardSet
import cs.mad.flashcards.entities.FlashcardSetDao
import cs.mad.flashcards.runOnIO
import kotlin.random.Random

class MainActivity : AppCompatActivity() {
    private lateinit var binding: ActivityMainBinding
    private lateinit var setDao: FlashcardSetDao

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        val db = Room.databaseBuilder(
            applicationContext,
            FlashcardSetDatabase::class.java, FlashcardSetDatabase.databaseName
        ).build()
        setDao = db.setDao()

        //prefs = getSharedPreferences("week5madflashcardsets", MODE_PRIVATE)
        binding.flashcardSetList.adapter = FlashcardSetAdapter(setDao)

        binding.createSetButton.setOnClickListener {
            runOnIO {
                setDao.insert(FlashcardSet("test",Random.nextInt(0,99999)))
            }
            (binding.flashcardSetList.adapter as FlashcardSetAdapter).addItem(FlashcardSet("test",123))
            binding.flashcardSetList.smoothScrollToPosition((binding.flashcardSetList.adapter as FlashcardSetAdapter).itemCount - 1)
        }

        // load data
        runOnIO {
            (binding.flashcardSetList.adapter as FlashcardSetAdapter).update(setDao.getAll())
        }

        // delete data
//        runOnIO {
//            setDao.deleteAll()
//        }

    }

}
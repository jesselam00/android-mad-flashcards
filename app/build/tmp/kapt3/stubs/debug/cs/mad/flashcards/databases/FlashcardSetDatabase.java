package cs.mad.flashcards.databases;

import java.lang.System;

@androidx.room.Database(entities = {cs.mad.flashcards.entities.FlashcardSet.class}, version = 1)
@kotlin.Metadata(mv = {1, 6, 0}, k = 1, d1 = {"\u0000\u0014\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\b\'\u0018\u0000 \u00052\u00020\u0001:\u0001\u0005B\u0005\u00a2\u0006\u0002\u0010\u0002J\b\u0010\u0003\u001a\u00020\u0004H&\u00a8\u0006\u0006"}, d2 = {"Lcs/mad/flashcards/databases/FlashcardSetDatabase;", "Landroidx/room/RoomDatabase;", "()V", "setDao", "Lcs/mad/flashcards/entities/FlashcardSetDao;", "Companion", "app_debug"})
public abstract class FlashcardSetDatabase extends androidx.room.RoomDatabase {
    @org.jetbrains.annotations.NotNull()
    public static final cs.mad.flashcards.databases.FlashcardSetDatabase.Companion Companion = null;
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String databaseName = "FLASHCARDSET_DATABASE";
    
    public FlashcardSetDatabase() {
        super();
    }
    
    @org.jetbrains.annotations.NotNull()
    public abstract cs.mad.flashcards.entities.FlashcardSetDao setDao();
    
    @kotlin.Metadata(mv = {1, 6, 0}, k = 1, d1 = {"\u0000\u0012\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0000\b\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002\u00a2\u0006\u0002\u0010\u0002R\u000e\u0010\u0003\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0005"}, d2 = {"Lcs/mad/flashcards/databases/FlashcardSetDatabase$Companion;", "", "()V", "databaseName", "", "app_debug"})
    public static final class Companion {
        
        private Companion() {
            super();
        }
    }
}